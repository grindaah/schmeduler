#coding: utf-8

from flask import render_template, flash, redirect
from app import app
from forms import LoginForm

import kinterbasdb
from datetime import datetime

user = { 'nickname': 'Victor' } # выдуманный пользователь
mydb = '/home/grindaah/learn/python/scheduler/tmp/LIST.FDB'
# База создана в Windows, кодировка cp1251

@app.route('/')
@app.route('/index')
def index():
    hospitals = []
    con = kinterbasdb.connect(host='localhost', database=mydb, user='sysdba', password='masterkey')
    cur = con.cursor()
    SELECT = "SELECT CODE, NAME FROM DIREC_CLINIC"

    cur.execute(SELECT)
    for row in cur:
        hospitals.append({"name": row[1], "desc": row[0]})
    return render_template("index.html",
        title = 'Home',
        user = user,
        hospitals = hospitals)

@app.route('/login', methods = ['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        print form.login_data
        return redirect('/index')
    return render_template('login.html',
        title = 'Sign In',
        form = form)

@app.route('/news')
def news():
    posts = [ # список выдуманных постов
        { 
            'author': { 'nickname': 'John' }, 
            'body': 'Beautiful day in Portland!' 
        },
        { 
            'author': { 'nickname': 'Susan' }, 
            'body': 'The Avengers movie was so cool!' 
        }
    ]
    return render_template("news.html",
        title = u'Новости',
        user = user,
        posts = posts)


@app.route('/get', methods = ['GET'])
def get():
    hospitals = [ # список мед учреждений
        { 
            'name': u'Введенская больница', 
            'desc': u'Введенский, 32' 
        },
        { 
            'name': u'Мариинская больница', 
            'desc': u'Литейный, 45' 
        },
        { 
            'name': u'НИИ им. Джанелидзе', 
            'desc': u'Софийская, 55' 
        }
    ]

@app.route('/docs', methods = ['GET'])
def docs():
    docs = []
    con = kinterbasdb.connect(host='localhost', database=mydb, user='sysdba', password='masterkey')
    cur = con.cursor()
    #hos_name = request.args.get('hos_name')
    hos_name = u"п1"
    SEL = u"SELECT D.CODE, D.NAME FROM LINK_DOCTOR L LEFT JOIN DIREC_DOCTOR D ON D.CODE=L.FK_DIREC_DOCTOR WHERE L.FK_DIREC_CLINIC='%s'" % hos_name
    SELECT = unicode(SEL).encode('cp1251')

    cur.execute(SELECT)
    for row in cur:
        docs.append({"name": row[1], "desc": row[0]})
    print docs
    return render_template("index.html",
        title = 'Home',
        user = user,
        hospitals = docs)

@app.route('/hospital_docs_<id>')
def hospital_docs(id):
    docs = []
    con = kinterbasdb.connect(host='localhost', database=mydb, user='sysdba', password='masterkey')
    cur = con.cursor()
    hos_name = unicode(id)
    SEL = u"SELECT DISTINCT S.CODE, S.NAME, D.CODE AS DOC_CODE, D.NAME AS DOC_NAME FROM LINK_DOCTOR L LEFT JOIN DIREC_SPECIALITY S ON S.CODE=L.FK_DIREC_SPECIALITY LEFT JOIN DIREC_DOCTOR D on D.CODE=L.fk_direc_doctor WHERE L.FK_DIREC_CLINIC='%s'" % id
    SELECT = unicode(SEL).encode('cp1251')

    cur.execute(SELECT)
    for row in cur:
        docs.append({"name": row[3], "desc": row[2], "spec": row[1], "spec_id": row[0], "hosp": id})
    print docs
    return render_template("hospital_docs.html",
        title = u'Доктора поликлиники',
        user = user,
        doctors = docs)

@app.route('/doc_<id>_<spec_id>_<hosp>')
def doc(id, spec_id, hosp):
    docs = []
    con = kinterbasdb.connect(host='localhost', database=mydb, user='sysdba', password='masterkey')
    cur = con.cursor()
    #id = unicode(id)
    #spec_id = unicode(spec_id)
    #hosp = unicode(hosp)

    dt = ".".join([str(datetime.now().date().day), str(datetime.now().date().month), str(datetime.now().date().year)])
    print dt
    SEL = u"SELECT * FROM PR_LIST_SELECT('%s', '%s', '%s', '%s')" % (dt, id, spec_id, hosp)
    print SEL
    SELECT = unicode(SEL).encode('cp1251')

    cur.execute(SELECT)
    for row in cur:
        docs.append({"slot_id": row[0], "slot": row[2], "long": row[3]})
    print docs
    return render_template("docs.html",
        title = u'Доктора поликлиники',
        user = user,
        doctors = docs)



#SELECT D.CODE, D.NAME
#FROM LINK_DOCTOR L
#LEFT JOIN DIREC_DOCTOR D ON D.CODE=L.FK_DIREC_DOCTOR
#WHERE L.FK_DIREC_CLINIC='п1'
#AND L.FK_DIREC_SPECIALITY='тер'
